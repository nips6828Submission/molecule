#include <Core/StartUp.h>
#define SERIALIZATIONS BOOST_SERIALIZATIONS

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/preprocessor/stringize.hpp>
#include <boost/filesystem.hpp>
#include <boost/serialization/deque.hpp>

#include <iostream>
#include <algorithm>
#include <boost/program_options.hpp>

#include "Core/Backward.h"

#include "Learning/SmileGRUGuacamol.h"
#include "Learning/ExperimentDataMoleculeGuacamol.h"
#include "Learning/ExperimentSmileGRUGuacamol.h"

#include "torch/csrc/api/include/torch/optim/adam.h"
#include "torch/csrc/api/include/torch/optim/sgd.h"
#include <future>
#include <queue>
#include <nvToolsExt.h>
#include <csignal>

StartUp start;

constexpr size_t nbThread=6;

std::atomic<bool> quit(false);

void signal_handler(int)
{
    quit.store(true);
}

int main(int argc, char *argv[])
{
    std::signal(SIGINT, signal_handler);
    std::signal(SIGTERM,signal_handler);
    std::signal(SIGABRT,signal_handler);
    int nn_depth;
    int nn_hidden;
    int gru_hidden;
    bool randomize;
    std::string NameData;
    std::string Name;
    int nbEpoch;
    size_t batchSize;
    double learningRate;
    size_t SaveEvery;
    std::string task;
    boost::program_options::options_description desc("Allowed options");
    desc.add_options()
    ("help", "produce help message")
    ("nnDepth",boost::program_options::value<int>(&nn_depth)->default_value(1),"Depth of the NN network predicting longitude and lattitude")
    ("nnHidden",boost::program_options::value<int>(&nn_hidden)->default_value(128),"Number of hidden unit of the NN network predicting longitude and lattitude")
    ("gruHidden",boost::program_options::value<int>(&gru_hidden)->default_value(128),"Number of hidden unit of the gru")
    ("randomize",boost::program_options::value<bool>(&randomize)->default_value(true),"If we randomize the LSTM.")
    ("name",boost::program_options::value<std::string>(&Name)->default_value("Default"),"Name of the experiment Run")
    ("nameData",boost::program_options::value<std::string>(&NameData)->default_value("Guacamol"),"Name of the Data Split to use")
    ("nbEpoch",boost::program_options::value<int>(&nbEpoch)->default_value(500),"Number of epoch to use.")
    ("learningRate",boost::program_options::value<double>(&learningRate)->default_value(1e-3),"Learning rate.")
    ("batchSize",boost::program_options::value<size_t>(&batchSize)->default_value(100),"Batch Size.")
    ("saveEvery",boost::program_options::value<size_t>(&SaveEvery)->default_value(10),"Every how many epoch to save the model.")
    ("task",boost::program_options::value<std::string>(&task)->default_value(std::string("")),"Task to solve. One of: mol_weight, logP, num_H_donors, tpsa, num_atoms.")
    ("check","check if the script was already run.");
    
    boost::program_options::variables_map vm;

    boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);
    
    boost::program_options::notify(vm);
        
    if (vm.count("help"))
    {
        std::cout << desc << "\n";
        return 0;
    }
    
    if(nbEpoch<=0)
    {
        throw std::runtime_error("Number of epoch cannot be negative or zero.");
    }
    if(nn_depth<0 || nn_hidden<=0 || gru_hidden<=0 )
    {
        throw std::runtime_error("The depth or number of unit canot be negatif or zero.");
    }
    
    ExperimentSmileGRUGuacamol Exp(nn_depth,nn_hidden,gru_hidden,Name,NameData,learningRate,randomize,task);
    if(Exp.AlreadyDone())
    {
        std::cout<<"Already done"<<std::endl;
        return 0;
    }
    else
    {
        if(vm.count("check"))
        {
            return 1;
        }
    }
    nvtxRangePushA("Loading data");
    std::cout<<"Loading Data"<<std::endl;
    auto expData=Exp.GetData();
    expData.Load();
    nvtxRangePop();
    
    boost::filesystem::create_directories(boost::filesystem::path{Exp.GetDirectoryPath()});
    std::ofstream ofs(Exp.GetDirectoryPath()+std::string("/Experiment.txt"));
    std::cout<<"Result written to: "<<Exp.GetDirectoryPath()+std::string("/Experiment.txt")<<std::endl;
    
    auto& DataTrain=expData.TrainData();
    
    auto& DataTest=expData.TestData();
    
    auto& DataVal=expData.ValData();

    ML<MoleculeGuacamol>::m_size=gru_hidden;
    typedef SmileGRUGuacamol Compute;
    
    nvtxRangePushA("Creating graph");
    std::vector<size_t> hidden_Form(nn_depth,nn_hidden);
    Compute Graph(hidden_Form,task,randomize);
    nvtxRangePop();
    
    auto param=Graph.GetTensor();
    auto adamOption=torch::optim::AdamOptions(learningRate);
    torch::optim::Adam adam(param,adamOption);
    
    
    std::chrono::steady_clock clock;
    typedef std::chrono::duration<double> duration_t;
    size_t nbBatch=(DataTrain.size()-1)/batchSize+1;
    size_t nbBatchTest=(DataTest.size()-1)/batchSize+1;
    size_t nbBatchVal=(DataVal.size()-1)/batchSize+1;
    if(DataVal.size()==0)
    {
        nbBatchVal=0;
    }
    
    auto funcFill=[&Graph,&DataTrain,&batchSize](size_t batch)
    {
        size_t start=batch*batchSize;
        size_t last=std::min((batch+1)*batchSize,DataTrain.size());
        return Graph.ToBatchTensor(DataTrain.begin()+start,DataTrain.begin()+last);
    };
    
    auto funcFillTest=[&Graph,&DataTest,&batchSize](size_t batch)
    {
        size_t start=batch*batchSize;
        size_t last=std::min((batch+1)*batchSize,DataTest.size());
        return Graph.ToBatchTensor(DataTest.begin()+start,DataTest.begin()+last);
    };
    
    auto funcFillVal=[&Graph,&DataVal,&batchSize](size_t batch)
    {
        size_t start=batch*batchSize;
        size_t last=std::min((batch+1)*batchSize,DataVal.size());
        return Graph.ToBatchTensor(DataVal.begin()+start,DataVal.begin()+last);
    };
    int i=1;
    std::queue<std::future<torch::jit::Stack>> queue;
    auto FillQueue=[&]()
    {
        for(size_t batch=0;batch<std::min(size_t(nbThread),nbBatch);++batch)
        {
            auto input=std::async(std::launch::async,funcFill,batch);
            queue.push(std::move(input));
        }
    };
    FillQueue();
    
    std::queue<std::future<torch::jit::Stack>> queueTest;
    auto FillQueueTest=[&]()
    {
        for(size_t batch=0;batch<std::min(size_t(nbThread),nbBatchTest);++batch)
        {
            auto input=std::async(std::launch::async,funcFillTest,batch);
            queueTest.push(std::move(input));
        }
    };
    FillQueueTest();
    
    std::queue<std::future<torch::jit::Stack>> queueVal;
    auto FillQueueVal=[&]()
    {
        for(size_t batch=0;batch<std::min(size_t(nbThread),nbBatchVal);++batch)
        {
            auto input=std::async(std::launch::async,funcFillVal,batch);
            queueVal.push(std::move(input));
        };
    };
    FillQueueVal();
    
    auto ComputeMean=[](auto list)
    {
        double mean=0;
        std::for_each(list.begin(),list.end(),[&mean](auto elem)
        {
            torch::autograd::Variable meanBlock=elem.mean();
            mean+=meanBlock.item<double>();
        });
        return mean/list.size();
    };
    
    auto ComputePearsonR2=[&ComputeMean,&clock](auto pred,auto target)
    {
        auto start_time = clock.now();
        double meanPred=ComputeMean(pred);
        double meanTarget=ComputeMean(target);
        double numerator=0;
        for(int i=0;i<pred.size();++i)
        {
            torch::autograd::Variable temp=(pred[i]-meanPred)*(target[i]-meanTarget);
            torch::autograd::Variable tempSum=temp.sum();
            numerator+=tempSum.item<double>();
        }
        double sumSquarePred=0;
        double sumSquareTarget=0;
        for(int i=0;i<pred.size();++i)
        {
            torch::autograd::Variable squaredPred=pow(pred[i]-meanPred,2).sum();
            sumSquarePred+=squaredPred.item<double>();
            torch::autograd::Variable squaredTarget=pow(target[i]-meanTarget,2).sum();
            sumSquareTarget+=squaredTarget.item<double>();
        }
        duration_t difftime=clock.now()-start_time;
        return pow(numerator,2)/(sumSquarePred*sumSquareTarget);
    };
    
    for(i=1;i<=nbEpoch;++i)
    {
        nvtxRangePushA("Epoch"); 
        double error_train;
        double error_test;
        double error_val;
        
        double pearson_train;
        double pearson_test;
        double pearson_val;
        
        double timePerInstance;
        
        double GradNorm;
        {
        nvtxRangePushA("Train");
        auto start_time = clock.now();
        double error=0;
        double weightNorm=0;
        size_t batch=std::min(size_t(nbThread),nbBatch);
        std::deque<torch::autograd::Variable> predList;
        std::deque<torch::autograd::Variable> targetList;
        while(batch<nbBatch)
        {
            adam.zero_grad();
            torch::autograd::Variable one=torch::autograd::make_variable(at::ones({1},at::kCUDA),false);
            nvtxRangePushA("GetInput");
            auto realInput=queue.front().get();
            nvtxRangePop();
            queue.pop();
            auto inputnew=std::async(std::launch::async,funcFill,batch);
            queue.push(std::move(inputnew));
            nvtxRangePushA("Forward"); 
            auto [errorVar,errorVarNotNorm,pred,label]=Graph.Execute(realInput);
            nvtxRangePop();
            nvtxRangePushA("Backward");
            Backward(errorVar,one);
            nvtxRangePop();
            nvtxRangePushA("Clipping");
            std::for_each(param.begin(),param.end(),[&weightNorm](auto elem)
            {
                if(elem.grad().defined())
                {
                    elem.grad().detach_();
                    elem.grad().clamp_(-5,5);
                    torch::autograd::Variable normWeight=elem.grad().norm();
                    weightNorm=std::max(normWeight.item<double>(),weightNorm);
                }
            });
            nvtxRangePop();
            adam.step();
            error+=errorVarNotNorm.item<double>();
            auto pred_cpu=pred.cpu().detach_();
            predList.push_back(pred_cpu);
            auto label_cpu=label.cpu().detach_();
            targetList.push_back(label_cpu);
            ++batch;
        }
        while(!queue.empty())
        {
            adam.zero_grad();
            torch::autograd::Variable one=torch::autograd::make_variable(at::ones({1},at::kCUDA),false);
            nvtxRangePushA("GetInput");
            auto realInput=queue.front().get();
            nvtxRangePop();
            queue.pop();
            nvtxRangePushA("Forward");
            auto [errorVar,errorVarNotNorm,pred,label]=Graph.Execute(realInput);
            nvtxRangePop();
            nvtxRangePushA("Backward");
            Backward(errorVar,one);
            nvtxRangePop();
            nvtxRangePushA("Clipping");
            std::for_each(param.begin(),param.end(),[&weightNorm](auto elem)
            {
                if(elem.grad().defined())
                {
                    elem.grad().detach_();
                    elem.grad().clamp_(-5,5);
                    torch::autograd::Variable normWeight=elem.grad().norm();
                    weightNorm=std::max(normWeight.item<double>(),weightNorm);
                }
            });
            nvtxRangePop();
            adam.step();
            error+=errorVarNotNorm.item<double>();
            ++batch;
            auto pred_cpu=pred.cpu().detach_();
            predList.push_back(pred_cpu);
            auto label_cpu=label.cpu().detach_();
            targetList.push_back(label_cpu);
        }
        nvtxRangePop();
        std::cout<<"epoch "<<i<<std::endl;
        duration_t difftime=clock.now()-start_time;
        double PearsonR2=ComputePearsonR2(predList,targetList);
        pearson_train=PearsonR2;
        std::cout<<"data size "<<DataTrain.size()<<std::endl;
        std::cout<<"total time "<<difftime.count()<<std::endl;
        timePerInstance=difftime.count()/(DataTrain.size());
        std::cout<<"time per instance "<<timePerInstance<<std::endl;
        std::cout<<"Error "<<error/static_cast<double>(DataTrain.size())<<std::endl;
        error_train=error/static_cast<double>(DataTrain.size());
        std::cout<<"Grad WeightNorm "<<weightNorm<<std::endl;
        std::cout<<"PearsonR2 "<<PearsonR2<<std::endl;
        GradNorm=weightNorm;
        std::random_shuffle(DataTrain.begin(),DataTrain.end());
        nvtxRangePushA("FillQueue");
        FillQueue();
        nvtxRangePop();
        }
        {
        std::deque<torch::autograd::Variable> predList;
        std::deque<torch::autograd::Variable> targetList;
        nvtxRangePushA("Test");
        auto start_time = clock.now();
        size_t batch=std::min(size_t(nbThread),nbBatchTest);
        double errortest=0;
        while(batch<nbBatchTest)
        {
            auto realInput=queueTest.front().get();
            queueTest.pop();
            auto inputnew=std::async(std::launch::async,funcFillTest,batch);
            queueTest.push(std::move(inputnew));
            auto [errorVar,errorVarNotNorm,pred,label]=Graph.Execute(realInput);
            errortest+=errorVarNotNorm.item<double>();
            ++batch;
            auto pred_cpu=pred.cpu().detach_();
            predList.push_back(pred_cpu);
            auto label_cpu=label.cpu().detach_();
            targetList.push_back(label_cpu);
        }
        while(!queueTest.empty())
        {
            auto realInput=queueTest.front().get();
            queueTest.pop();
            auto [errorVar,errorVarNotNorm,pred,label]=Graph.Execute(realInput);
            errortest+=errorVarNotNorm.item<double>();
            auto pred_cpu=pred.cpu().detach_();
            predList.push_back(pred_cpu);
            auto label_cpu=label.cpu().detach_();
            targetList.push_back(label_cpu);
        }
        duration_t difftime=clock.now()-start_time;
        nvtxRangePop();
        std::cout<<"bef PearsonR2"<<std::endl;
        double PearsonR2=ComputePearsonR2(predList,targetList);
        pearson_test=PearsonR2;
        std::cout<<"data size "<<DataTest.size()<<std::endl;
        std::cout<<"total time "<<difftime.count()<<std::endl;
        std::cout<<"Error Test "<<errortest/static_cast<double>(DataTest.size())<<std::endl;
        std::cout<<"Pearson Test "<<pearson_test<<std::endl;
        error_test=errortest/static_cast<double>(DataTest.size());
        FillQueueTest();
        }
        
        {
        std::deque<torch::autograd::Variable> predList;
        std::deque<torch::autograd::Variable> targetList;
        nvtxRangePushA("Val");
        auto start_time = clock.now();
        size_t batch=std::min(size_t(nbThread),nbBatchVal);
        double errorVal=0;
        double accVal=0;
        while(batch<nbBatchVal)
        {
            auto realInput=queueVal.front().get();
            queueVal.pop();
            auto inputnew=std::async(std::launch::async,funcFillVal,batch);
            queueVal.push(std::move(inputnew));
            auto [errorVar,errorVarNotNorm,pred,label]=Graph.Execute(realInput);
            errorVal+=errorVarNotNorm.item<double>();
            ++batch;
            auto pred_cpu=pred.cpu().detach_();
            predList.push_back(pred_cpu);
            auto label_cpu=label.cpu().detach_();
            targetList.push_back(label_cpu);
        }
        while(!queueVal.empty())
        {
            auto realInput=queueVal.front().get();
            queueVal.pop();
            auto [errorVar,errorVarNotNorm,pred,label]=Graph.Execute(realInput);
            errorVal+=errorVarNotNorm.item<double>();
            auto pred_cpu=pred.cpu().detach_();
            predList.push_back(pred_cpu);
            auto label_cpu=label.cpu().detach_();
            targetList.push_back(label_cpu);
        }
        duration_t difftime=clock.now()-start_time;
        nvtxRangePop();
        double PearsonR2=ComputePearsonR2(predList,targetList);
        pearson_val=PearsonR2;
        std::cout<<"data size "<<DataVal.size()<<std::endl;
        std::cout<<"total time "<<difftime.count()<<std::endl;
        std::cout<<"Error Val "<<errorVal/static_cast<double>(DataVal.size())<<std::endl;
        std::cout<<"Pearson val "<<pearson_val<<std::endl;
        error_val=errorVal/static_cast<double>(DataVal.size());
        FillQueueVal();
        }
        nvtxRangePushA("Saving To Disk");
        ofs<<i<<" "<<error_train<<" "<<error_test<<" "<<error_val<<" "<<pearson_train<<" "<<pearson_test<<" "<<pearson_val<<" "<<GradNorm<<" "<<timePerInstance<<std::endl;
        if(i%SaveEvery==0)
        {
        torch::serialize::OutputArchive OAOptimizer;
        adam.save(OAOptimizer);
        torch::serialize::OutputArchive OA;
        OA.write("Optimizer",OAOptimizer);
        auto model=Graph.Save();
        OA.write("Model",model);
        OA.save_to(Exp.GetDirectoryPath()+std::string("/")+std::to_string(i));
        }
        nvtxRangePop();
        nvtxRangePop();
        if(quit.load())
        {
            std::cout<<"Receiving stop instruction"<<std::endl;
            break;
        }
    }
    Exp.SetDone();
    {
        torch::serialize::OutputArchive OAOptimizer;
        adam.save(OAOptimizer);
        torch::serialize::OutputArchive OA;
        OA.write("Optimizer",OAOptimizer);
        auto model=Graph.Save();
        OA.write("Model",model);
        OA.save_to(Exp.GetDirectoryPath()+std::string("/FinalModel"));
    }
    std::cout<<"Execution finished"<<std::endl;
}



