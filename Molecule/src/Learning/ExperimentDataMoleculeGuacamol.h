#pragma once

#include "Core/ExperimentData.h"
#include "ExperimentDataMoleculeGuacamol.h"
#include <deque>
#include <string>
#include <boost/filesystem.hpp>
#include <Molecule/MoleculeGuacamol.h>

class ExperimentDataMoleculeGuacamol : public ExperimentData<std::deque<MoleculeGuacamol>>
{
public:
    ExperimentDataMoleculeGuacamol(std::string dataName): ExperimentData<std::deque<MoleculeGuacamol>>(dataName)
    {
    }
    virtual std::string GetDirectoryPath() const
    {
        return std::string("/Data/Data/Molecule/")+this->GetDataName();
    }
    virtual void Load(bool setPoly=true)
    {
        TrainData()=CSV2MoleculeGuacamol(std::string("/Data/Data/Molecule/")+GetDataName()+std::string("/guacamol_v1_train.smiles"));
        TestData()=CSV2MoleculeGuacamol(std::string("/Data/Data/Molecule/")+GetDataName()+std::string("/guacamol_v1_test.smiles"));
        ValData()=CSV2MoleculeGuacamol(std::string("/Data/Data/Molecule/")+GetDataName()+std::string("/guacamol_v1_valid.smiles"));
    };
    virtual void Save() const
    {
    };
    using ExperimentData<std::deque<MoleculeGuacamol>>::type_data;
};

