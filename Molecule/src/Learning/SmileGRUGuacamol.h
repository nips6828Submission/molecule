#pragma once
#include "Core/FunctionOp.h"
#include "Molecule/MoleculeGuacamol.h"
#include "Molecule/InputSymbolGuacamol.h"
#include "torch/csrc/jit/batched/BatchTensor.h"
#include "Builder/MLBuilder.h"
#include "Builder/MLBuilderMLP.h"
#include "Builder/MLBuilderGRU.h"
#include <nvToolsExt.h>

/**
 * @brief Class that create the computation graph to Solve the Orbiwise problem.
 */
class SmileGRUGuacamol
{
    protected:
    /**
     * @brief Builder for normal neural network.
     */
    MLBuilderMLP<> m_Builder;
    /**
     * @brief Builder for GRU 
     */
    MLBuilderGRU<> m_BuilderGRU;
    /**
     * @brief AddSet
     */
    FunctionOp<std::function<ML<MoleculeGuacamol>(ML<MoleculeGuacamol>,ML<InputSymbolGuacamol>)>> m_AddFTime;
    /**
     * @brief Function to create initial state.
     */
    FunctionOp<std::function<ML<MoleculeGuacamol>()>> m_Constructor;
    /**
     * @brief Function classify.
     */
    FunctionOp<std::function<ML<double>(ML<MoleculeGuacamol>)>> m_GetLabel;
    /**
     * @brief Storage of the graph Executor;
     */
    torch::jit::GraphExecutor m_executor;
    /**
     * @brief vector of Parameter.
     * */
    std::vector<at::Tensor> m_paramTensor;
    
    std::string m_task;
    
    bool m_randomize=true;
    /**
     * @brief List of Tensors containing information about one instance.
     */
    struct TensorsLists
    {
        at::Tensor Value;
        at::Tensor Size;
        at::Tensor Label;
    };
    
    /**
     * @brief Structure containing value returned.
     */
    struct RunReturn
    {
        torch::autograd::Variable sum;
        torch::autograd::Variable sumNotNorm;
        torch::autograd::Variable pred;
        torch::autograd::Variable label;
    };
    
    /**
     * @brief structure used to return value for the evaluation.
     */
    struct PredReturn
    {
        double Pred;
        double Target;
    };
    
    /**
     * @brief Function that create a single tensor.
     */
    TensorsLists SingleToTensor(const MoleculeGuacamol& mol)
    {
        TensorsLists ret;
        std::string molecule;
        if(m_randomize)
        {
            molecule=mol.Sample();
        }
        else
        {
            molecule=mol.m_smile;
        }
        size_t size=molecule.size();
        ret.Size=at::empty({1,1},at::CUDA(at::kInt));
        ret.Size[0][0]=int(size-1);
        ret.Value=at::zeros({1,size,MoleculeGuacamol::symbols.size()}, at::kFloat);
        auto Accessor=ret.Value.packed_accessor<float,3>();
        for(size_t i=0;i<size;++i)
        {
            auto tensor=Accessor[0][i];
            InputSymbolGuacamol input(molecule[i]);
            m_Builder.GetML(input,tensor);
        }
        ret.Value=ret.Value.cuda();
        ret.Label=at::zeros({1,1},at::kCUDA);
        m_Builder.GetML(mol.Get(m_task),ret.Label[0]);
        return ret;
    };
    
public:
    /**
     * @brief Get the param Tensor.
     */
    std::vector<at::Tensor>& GetTensor()
    {
        return m_paramTensor;
    };
    
    /**
     * @brief Transform to Batch Tensor.
     */
    template <typename Iterator>
    torch::jit::Stack ToBatchTensor(Iterator begin,Iterator end)
    {
        nvtxRangePushA("BatchToTensor");
        std::vector<at::Tensor> Value;
        std::vector<at::Tensor> Size;
        std::vector<at::Tensor> Label;
        static size_t nbiter=0;
        nvtxRangePushA("Generate");
        std::for_each(begin,end,[&](auto& elem)
        {
            nvtxRangePushA("CreateTensors");
            auto [tenValue,tenSize,tenLabel]=SingleToTensor(elem);
            nvtxRangePop();
            Value.push_back(tenValue);
            Size.push_back(tenSize);
            Label.push_back(tenLabel);
        }
        );
        nvtxRangePop();
        nvtxRangePushA("CreateStack");
        auto StaticDim=at::ones({1},at::kByte);
        StaticDim[0]=false;
        auto DynamicDim=at::ones({2},at::kByte);
        DynamicDim[0]=true;
        DynamicDim[1]=false;
        
        auto SameDim=at::ones({1},at::kByte);
        SameDim[0]=true;
        
        torch::jit::BatchTensor BatchValue(Value,DynamicDim);
        torch::jit::BatchTensor BatchSize(Size,StaticDim);
        torch::jit::BatchTensor BatchLabel(Label,StaticDim);
        
        auto funcVar=[](auto elem)
        {
            return torch::autograd::make_variable(elem.to(at::Device("cuda")),false);
        };
        
        torch::jit::Stack stack({funcVar(BatchValue.get_data()),funcVar(BatchValue.get_mask()),funcVar(BatchValue.get_dims()),funcVar(BatchSize.get_data()),funcVar(BatchSize.get_mask()),funcVar(BatchSize.get_dims()),funcVar(BatchLabel.get_data()),funcVar(BatchLabel.get_mask()),funcVar(BatchLabel.get_dims())});
                
        auto batchParam=[](auto elem)
        {
            torch::jit::Stack res;
            std::for_each(elem.begin(),elem.end(),[&res](auto elem2)
            {
                auto var=elem2.toTensor();
                var.set_requires_grad(true);
                torch::jit::BatchTensor batch(var,size_t(1));
                res.push_back(batch.get_data());
                res.push_back(batch.get_mask());
                res.push_back(batch.get_dims());
            });
            return res;
        };
    
        auto ParamConstructor=batchParam(m_Constructor.GetParam());
        stack.insert(stack.end(),ParamConstructor.begin(),ParamConstructor.end());
        
        auto ParamAddFTime=batchParam(m_AddFTime.GetParam());
        stack.insert(stack.end(),ParamAddFTime.begin(),ParamAddFTime.end());
    
        auto ParamLabel=batchParam(m_GetLabel.GetParam());
        stack.insert(stack.end(),ParamLabel.begin(),ParamLabel.end());
        
        nvtxRangePop();
        nvtxRangePop();
        return stack;
    };
    
    /**
     * @brief Constructor of the computational graph.
     */
    SmileGRUGuacamol(std::vector<size_t> nn_hidden,std::string task, bool randomize) :m_Builder(at::CUDA(at::kFloat),nn_hidden),m_BuilderGRU(at::CUDA(at::kFloat)),m_task(task),m_randomize(randomize)
    {
        std::function<MoleculeGuacamol(MoleculeGuacamol,InputSymbolGuacamol)> fAddFTime=[](MoleculeGuacamol mol,InputSymbolGuacamol point)
        {
            return mol;
        };
        m_AddFTime=m_BuilderGRU.Build(fAddFTime);
        
        std::function<MoleculeGuacamol()> fConstructor=[]()
        {
            return MoleculeGuacamol();
        };
        m_Constructor=m_Builder.Build(fConstructor);
        
        std::function<double(MoleculeGuacamol)> fGetLabel=[](MoleculeGuacamol)
        {
            return 0.0;
        };
        m_GetLabel=m_Builder.Build(fGetLabel);
        
        torch::jit::Stack param;
        
        auto AddFTimeParam=m_AddFTime.GetParam();
        param.insert(param.end(),AddFTimeParam.begin(),AddFTimeParam.end());
        
        auto ConstructorParam=m_Constructor.GetParam();
        param.insert(param.end(),ConstructorParam.begin(),ConstructorParam.end());
        
        auto GetLabelParam=m_GetLabel.GetParam();
        param.insert(param.end(),GetLabelParam.begin(),GetLabelParam.end());
       
        std::transform(param.begin(),param.end(),std::back_inserter(m_paramTensor),[](auto elem)
        {
            return elem.toTensor();
        });
        
        auto graph=std::make_shared<torch::jit::Graph>();
        torch::jit::SymbolicVariable Values = torch::jit::SymbolicVariable::asNewInput(*graph);
        torch::jit::SymbolicVariable Size = torch::jit::SymbolicVariable::asNewInput(*graph);
        torch::jit::SymbolicVariable Label = torch::jit::SymbolicVariable::asNewInput(*graph);
        
        auto paramConstructor=m_Constructor.CreateSymbolicParameters(graph);
        
        auto paramAddFTime=m_AddFTime.CreateSymbolicParameters(graph);
        
        auto paramGetLabel=m_GetLabel.CreateSymbolicParameters(graph);
        
        torch::jit::SymbolicVariable stateEmpty=m_Constructor.InsertNode(graph,paramConstructor,{})[0];
        
        auto oneConstant=graph->insertConstant(1);
        auto falseConstant=graph->insertConstant(false);
        auto negoneConstant=graph->insertConstant(-1);
        auto zeroConstant=graph->insertConstant(at::zeros({},at::kCUDA));
        auto cond=Size>torch::jit::SymbolicVariable(negoneConstant);
        
        auto MaxLoop=graph->insertConstant(at::ones({},at::kCUDA)*10000);
        
        auto loop=graph->insertNode(graph->create(torch::jit::prim::Loop,1));
        loop->addInput(MaxLoop);
        loop->addInput(cond);
        loop->addInput(stateEmpty);
        {
            auto body=loop->addBlock();
            auto counter=body->insertInput(0);
            auto state=body->insertInput(1);
            torch::jit::WithInsertPoint guard(body);
            
            auto select=body->appendNode(graph->create(torch::jit::aten::select,1));
            select->addInput(Values);
            select->addInput(oneConstant);
            select->addInput(counter);
            auto output_select=torch::jit::SymbolicVariable(select->outputs()[0]);
        
            auto new_state=m_AddFTime.InsertNode(body->owningGraph(),paramAddFTime,{state,output_select})[0];
            
            auto cond=Size>torch::jit::SymbolicVariable(counter);
            body->registerOutput(cond);
            body->registerOutput(new_state);
        }
          
        auto output_state=loop->outputs()[0];
        
        auto PredAct=m_GetLabel.InsertNode(graph,paramGetLabel,{output_state})[0];
        
        auto diff=PredAct-Label;
        
        auto error=diff*diff;
        auto errorSum=error.sum();
        
        graph->registerOutput(errorSum);
        graph->registerOutput(PredAct);
        graph->registerOutput(Label);
        graph->lint();
        auto graphBatch=to_batch_graph(graph);
        auto graphFinal=std::make_shared<torch::jit::Graph>();
        
        for(int i=0;i<graphBatch->inputs().size();++i)
        {
            graphFinal->addInput(graphBatch->inputs()[i]->uniqueName());
        }
        auto results=torch::jit::inlineCallTo(*graphFinal,*graphBatch,graphFinal->inputs(),true);
        auto resultData=results[0];
        auto resultMask=results[1];
        auto resultDim=results[2];
        auto result2Data=results[3];
        auto result2Mask=results[4];
        auto result2Dim=results[5];
        auto result3Data=results[6];
        auto result3Mask=results[7];
        auto result3Dim=results[8];
        auto afterMask=static_cast<torch::jit::SymbolicVariable>(resultData)*static_cast<torch::jit::SymbolicVariable>(resultMask).type_as(resultData);
        auto afterMask2=static_cast<torch::jit::SymbolicVariable>(result2Data)*static_cast<torch::jit::SymbolicVariable>(result2Mask).type_as(result2Data);
        auto afterMask3=static_cast<torch::jit::SymbolicVariable>(result3Data)*static_cast<torch::jit::SymbolicVariable>(result3Mask).type_as(result2Data);
        auto oneConstantFinal=graphFinal->insertConstant(0);
        auto size_Node=graphFinal->insertNode(graphFinal->create(torch::jit::aten::size,1));
        size_Node->addInput(afterMask);
        size_Node->addInput(oneConstantFinal);
        auto final_Size=size_Node->outputs()[0];
        auto valSum=static_cast<torch::jit::SymbolicVariable>(afterMask).sum();
        torch::jit::SymbolicVariable(valSum).addAsOutput();
        torch::jit::SymbolicVariable(final_Size).addAsOutput();
        torch::jit::SymbolicVariable(afterMask2).addAsOutput();
        torch::jit::SymbolicVariable(afterMask3).addAsOutput();
        graphFinal->lint();
        m_executor=torch::jit::GraphExecutor(graphFinal,true);
    };
    RunReturn Execute(torch::jit::Stack stack)
    {
        RunReturn ret;
        m_executor.run(stack);
        int nb=stack[1].toInt();
        ret.sum=stack[0].toTensor()/nb;
        ret.sumNotNorm=stack[0].toTensor();
        ret.pred=stack[2].toTensor();
        ret.label=stack[3].toTensor();
        return ret;
    };

    /*PredReturn Predict(const Message& m)
    {
        auto fAddFTime=m_AddFTime.GetFunction();
        auto fGetLat=m_GetLat.GetFunction();
        auto fGetLong=m_GetLong.GetFunction();
        auto setFTime=m.GetSetFTime();
        ML<Message> state;
        if(m_useTril)
        {
            auto fConstructor=m_ConstructorTril.value().GetFunction();
            state=fConstructor(m_Builder.GetML(m.GetTrilLatitude()),m_Builder.GetML(m.GetTrilLongitude()));
        }
        else
        {
            auto fConstructor=m_Constructor.value().GetFunction();
            state=fConstructor();
        }
        for(auto elem : setFTime)
        {
            state=fAddFTime(state,m_Builder.GetML(elem),m_Builder.GetML(m.GetFTime(elem)/m_timeScale));
        }
        double Lat=static_cast<double>(fGetLat(state));
        double Long=static_cast<double>(fGetLong(state));
        PredReturn ret;
        ret.PredLong=Long;
        ret.PredLat=Lat;
        ret.TargetLong=m.GetLongitude();
        ret.TargetLat=m.GetLatitude();
        ret.TrilLong=m.GetTrilLongitude();
        ret.TrilLat=m.GetTrilLatitude();
        return ret;
    }*/
    
    
    /**
     * @brief Load member function which update the parameters with a version loaded from the archive.
     * @param archive Archive were the parameters are stored.
     */
    void Load(torch::serialize::InputArchive& archive)
    {
        auto loadElem=[&archive](std::string Name,auto& functionOp)
        {
            torch::serialize::InputArchive archiveTemp;
            archive.read(Name,archiveTemp);
            functionOp.Load(archiveTemp);
        };
        loadElem("Constructor",m_Constructor);
        loadElem("AddFTime",m_AddFTime);
        loadElem("GetLabel",m_GetLabel);
    }
    /**
     * @brief Save member function which save the current parameter and return and archive that can be used to save the content to disc.
     */
    torch::serialize::OutputArchive Save() const
    {
        torch::serialize::OutputArchive output;
        auto saveElem=[&output](std::string Name,auto& functionOp)
        {
            auto archive=functionOp.Save();
            output.write(Name,archive);
        };
        saveElem("Constructor",m_Constructor);
        saveElem("AddFTime",m_AddFTime);
        saveElem("GetLabel",m_GetLabel);
        return output;
    }
};




