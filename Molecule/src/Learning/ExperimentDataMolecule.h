#pragma once

#include "Core/ExperimentData.h"
#include "ExperimentDataMolecule.h"
#include <deque>
#include <string>
#include <boost/filesystem.hpp>
#include <Molecule/Molecule.h>

class ExperimentDataMolecule : public ExperimentData<std::deque<Molecule>>
{
public:
    ExperimentDataMolecule(std::string dataName): ExperimentData<std::deque<Molecule>>(dataName)
    {
    }
    virtual std::string GetDirectoryPath() const
    {
        return std::string("/Data/Data/Molecule/")+this->GetDataName();
    }
    virtual void Load(bool setPoly=true)
    {
        TrainData()=CSV2Molecule(std::string("/Data/Data/Molecule/")+GetDataName()+std::string("/Train.csv"));
        TestData()=CSV2Molecule(std::string("/Data/Data/Molecule/")+GetDataName()+std::string("/Test.csv"));
        ValData()=CSV2Molecule(std::string("/Data/Data/Molecule/")+GetDataName()+std::string("/Val.csv"));
    };
    virtual void Save() const
    {
    };
    using ExperimentData<std::deque<Molecule>>::type_data;
};


