#pragma once
#include "Core/Experiment.h"
#include <deque>
#include "ExperimentDataMolecule.h"

class ExperimentSmileGRU : public Experiment<ExperimentDataMolecule>
{
    int m_nnLayer;
    int m_nnHidden;
    int m_gruHidden;
    bool m_randomize;
    double m_learningRate;
    std::string m_task;
    
public:
    ExperimentSmileGRU(int nnLayer,int nnHidden,int gruHidden,std::string name, std::string nameData,double learningRate,bool bRandomize,std::string task): Experiment<ExperimentDataMolecule>(name,nameData),m_nnLayer(nnLayer),m_nnHidden(nnHidden),m_gruHidden(gruHidden),m_learningRate(learningRate),m_randomize(bRandomize),m_task(task)
    {
    };
    
    void PrintDescription() const
    {
        Experiment<ExperimentDataMolecule>::PrintDescription();
        std::cout<<"nnLayer: "<<m_nnLayer<<std::endl;
        std::cout<<"nnHidden: "<<m_nnHidden<<std::endl;
        std::cout<<"gruHidden: "<<m_gruHidden<<std::endl;
        std::cout<<"LearningRate: "<<m_learningRate<<std::endl;
        std::cout<<"Randomize: "<<m_randomize<<std::endl;
        std::cout<<"Task: "<<m_task<<std::endl;
    };
    std::string GetDirectoryPath() const
    {
        std::string nameParam=std::to_string(m_nnLayer)+std::string("_")+std::to_string(m_nnHidden)+std::string("_")+std::to_string(m_gruHidden)+std::string("_")+std::string("_")+std::to_string(m_learningRate)+std::string("_")+m_task;
        if(m_randomize)
        {
            nameParam+="_R";
        }
        return std::string("/Data/Result/Molecule/SmileGRU/")+GetName()+std::string("/")+GetDataName()+std::string("/")+nameParam;
    };
};



