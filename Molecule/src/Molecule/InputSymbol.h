#pragma once
#include "Molecule/Molecule.h"
struct InputSymbol
{
    size_t m_id;
    InputSymbol(char character)
    {
        std::string str;
        str.push_back(character);
        m_id=Molecule::symbols[str];
    };
    static size_t size()
    {
        return Molecule::symbols.size();
    }
};

template <>
class ML<InputSymbol>: public at::Tensor
{
    public:
    static size_t size()
    {
        return Molecule::symbols.size();
    }
    ML()
    {
        
    }
    ML(at::Tensor embedded):at::Tensor(embedded)
    {
        
    }
    static constexpr bool IsFloatCompatible()
    {
	return false;
    }
    typedef InputSymbol Type;
};

auto GenerateMLValue(const InputSymbol& res,at::Tensor tensor)
{
    tensor[res.m_id]=1;
    return ML<InputSymbol>(tensor);
};

template <typename Accessor>
void GenerateMLValue(const InputSymbol& res,Accessor tensor)
{
    tensor[res.m_id]=1;
};
