#pragma once
#include <rdkit/GraphMol/SmilesParse/SmilesParse.h>
#include <GraphMol/MolStandardize/MolStandardize.h>
#include <RDGeneral/Invariant.h>
#include <GraphMol/RDKitBase.h>
#include <GraphMol/ROMol.h>
#include <GraphMol/SmilesParse/SmilesParse.h>
#include <GraphMol/SmilesParse/SmilesWrite.h>
#include <GraphMol/FileParsers/FileParsers.h>
#include <RDGeneral/FileParseException.h>
#include <GraphMol/FileParsers/MolSupplier.h>
#include <set>
#include <fstream>
#include <Core/ML.h>

class Molecule
{
public:
    std::string m_smile;
    std::shared_ptr<RDKit::ROMol> m_mol;
    double mu;
    double alpha;
    double homo;
    double lumo;
    double gap;
    double r2;
    double zpve;
    double cv;
    double u0;
    double u298;
    double h298;
    double g298;
    static std::map<std::string,size_t> symbols;
    std::string Sample() const
    {
        return RDKit::MolToSmiles(*m_mol,true,false,-1,true,false,false,true);
    };
    double Get(std::string task) const
    {
        if(task==std::string("mu"))
        {
            return mu;
        }
        else if(task==std::string("alpha"))
        {
            return alpha;
        }
        else if(task==std::string("homo"))
        {
            return homo;
        }
        else if(task==std::string("lumo"))
        {
            return lumo;
        }
        else if(task==std::string("gap"))
        {
            return gap;
        }
        else if(task==std::string("r2"))
        {
            return r2;
        }
        else if(task==std::string("zpve"))
        {
            return zpve;
        }
        else if(task==std::string("cv"))
        {
            return cv;
        }
        else if(task==std::string("u0"))
        {
            return u0;
        }
        else if(task==std::string("u298"))
        {
            return u298;
        }
        else if(task==std::string("h298"))
        {
            return h298;
        }
        else if(task==std::string("g298"))
        {
            return g298;
        }
        else
        {
            throw std::runtime_error(std::string("Unknown task ")+task);
        }
    }
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {
        ar & m_smile;
        ar & mu;
        ar & alpha;
        ar & homo;
        ar & lumo;
        ar & gap;
        ar & r2;
        ar & zpve;
        ar & cv;
        ar & u0;
        ar & u298;
        ar & h298;
        ar & g298;
    }
};

std::map<std::string,size_t> Molecule::symbols;

#include <boost/tokenizer.hpp>

std::deque<Molecule> CSV2Molecule(std::string file)
{
    std::deque<Molecule> res;
    std::ifstream in(file);
    if(!in.is_open())
    {
        throw std::runtime_error("Error in opening file");
    }
    typedef boost::tokenizer< boost::escaped_list_separator<char> > Tokenizer;
    std::string lineIn;
    while(getline(in,lineIn))
    {
        Tokenizer tokIn(lineIn);
        int i=0;
        Molecule cur_Molecule;
        std::for_each(tokIn.begin(),tokIn.end(),[&i,&cur_Molecule](auto elem)
        {
            switch(i)
            {
                case 0:
                    cur_Molecule.m_smile=elem;
                    cur_Molecule.m_mol=std::shared_ptr<RDKit::ROMol>(RDKit::SmilesToMol(elem));
                    std::for_each(elem.begin(),elem.end(),[](auto str)
                    {
                        std::string s;
                        s.push_back(str);
                        if(Molecule::symbols.count(s)==0)
                        {
                            Molecule::symbols[s]=Molecule::symbols.size();
                        }
                    });
                    for(int i=0;i<4;++i)
                    {
                        std::string str=RDKit::MolToSmiles(*cur_Molecule.m_mol,true,false,-1,true,false,false,true);
                        std::for_each(str.begin(),str.end(),[](auto str)
                        {
                            std::string s;
                            s.push_back(str);
                            if(Molecule::symbols.count(s)==0)
                            {
                                Molecule::symbols[s]=Molecule::symbols.size();
                            }
                        });
                    }
                    break;
                case 1:
                    cur_Molecule.mu=std::stod(elem);
                    break;
                case 2:
                    cur_Molecule.alpha=std::stod(elem);
                    break;
                case 3:
                    cur_Molecule.homo=std::stod(elem);
                    break;
                case 4:
                    cur_Molecule.lumo=std::stod(elem);
                    break;
                case 5:
                    cur_Molecule.gap=std::stod(elem);
                    break;
                case 6:
                    cur_Molecule.r2=std::stod(elem);
                    break;
                case 7:
                    cur_Molecule.zpve=std::stod(elem);
                    break;
                case 8:
                    cur_Molecule.cv=std::stod(elem);
                    break;
                case 9:
                    cur_Molecule.u0=std::stod(elem);
                    break;
                case 10:
                    cur_Molecule.u298=std::stod(elem);
                    break;
                case 11:
                    cur_Molecule.h298=std::stod(elem);
                    break;
                case 12:
                    cur_Molecule.g298=std::stod(elem);
                    break;
            }
            ++i;
        });
        res.push_back(cur_Molecule);
    }
    return res;
};

template <>
class ML<Molecule>: public at::Tensor
{
    public:
    static size_t m_size;
    static size_t size()
    {
        return m_size;
    }
    ML()
    {
        
    }
    ML(at::Tensor embedded):at::Tensor(embedded)
    {
        
    }
    static constexpr bool IsFloatCompatible()
    {
        return false;
    }
    typedef Molecule Type;
};

size_t ML<Molecule>::m_size=1024;

auto GenerateMLValue(Molecule res,at::Tensor tensor)
{
    return ML<Molecule>(tensor);
};

template <typename Accessor>
void GenerateMLValue(Molecule res,Accessor tensor)
{
};
