#pragma once
#include <rdkit/GraphMol/SmilesParse/SmilesParse.h>
#include <GraphMol/MolStandardize/MolStandardize.h>
#include <RDGeneral/Invariant.h>
#include <GraphMol/RDKitBase.h>
#include <GraphMol/ROMol.h>
#include <GraphMol/SmilesParse/SmilesParse.h>
#include <GraphMol/SmilesParse/SmilesWrite.h>
#include <GraphMol/FileParsers/FileParsers.h>
#include <RDGeneral/FileParseException.h>
#include <GraphMol/FileParsers/MolSupplier.h>
#include <GraphMol/Descriptors/MolDescriptors.h>
#include <set>
#include <fstream>
#include <Core/ML.h>

class MoleculeGuacamol
{
public:
    std::string m_smile;
    std::shared_ptr<RDKit::ROMol> m_mol;
    static std::map<std::string,size_t> symbols;
    std::string Sample() const
    {
        return RDKit::MolToSmiles(*m_mol,true,false,-1,true,false,false,true);
    };
    double Get(std::string task) const
    {
        if(task==std::string("mol_weight"))
        {
            return RDKit::Descriptors::calcAMW(*m_mol);
        }
        else if(task==std::string("logP"))
        {
            return RDKit::Descriptors::calcClogP(*m_mol);
        }
        else if(task==std::string("num_H_donors"))
        {
            return RDKit::Descriptors::calcNumHBD(*m_mol);
        }
        else if(task==std::string("tpsa"))
        {
            return RDKit::Descriptors::calcTPSA(*m_mol);
        }
        else if(task==std::string("num_atoms"))
        {
           std::shared_ptr<RDKit::ROMol> mol_temp(RDKit::MolOps::addHs(*m_mol));
           return mol_temp->getNumAtoms();
        }
        else
        {
            throw std::runtime_error(std::string("Unknown task ")+task);
        }
    }
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {
        ar & m_smile;
    }
};

std::map<std::string,size_t> MoleculeGuacamol::symbols;

#include <boost/tokenizer.hpp>

std::deque<MoleculeGuacamol> CSV2MoleculeGuacamol(std::string file)
{
    std::deque<MoleculeGuacamol> res;
    std::ifstream in(file);
    if(!in.is_open())
    {
        throw std::runtime_error("Error in opening file");
    }
    typedef boost::tokenizer< boost::escaped_list_separator<char> > Tokenizer;
    std::string lineIn;
    while(getline(in,lineIn))
    {
        Tokenizer tokIn(lineIn);
        int i=0;
        MoleculeGuacamol cur_Molecule;
        std::for_each(tokIn.begin(),tokIn.end(),[&i,&cur_Molecule](auto elem)
        {
            switch(i)
            {
                case 0:
                    cur_Molecule.m_smile=elem;
                    cur_Molecule.m_mol=std::shared_ptr<RDKit::ROMol>(RDKit::SmilesToMol(elem));
                    std::for_each(elem.begin(),elem.end(),[](auto str)
                    {
                        std::string s;
                        s.push_back(str);
                        if(MoleculeGuacamol::symbols.count(s)==0)
                        {
                            MoleculeGuacamol::symbols[s]=MoleculeGuacamol::symbols.size();
                        }
                    });
                    for(int i=0;i<4;++i)
                    {
                        std::string str=RDKit::MolToSmiles(*cur_Molecule.m_mol,true,false,-1,true,false,false,true);
                        std::for_each(str.begin(),str.end(),[](auto str)
                        {
                            std::string s;
                            s.push_back(str);
                            if(MoleculeGuacamol::symbols.count(s)==0)
                            {
                                MoleculeGuacamol::symbols[s]=MoleculeGuacamol::symbols.size();
                            }
                        });
                    }
                    break;
            }
            ++i;
        });
        res.push_back(cur_Molecule);
    }
    return res;
};

template <>
class ML<MoleculeGuacamol>: public at::Tensor
{
    public:
    static size_t m_size;
    static size_t size()
    {
        return m_size;
    }
    ML()
    {
        
    }
    ML(at::Tensor embedded):at::Tensor(embedded)
    {
        
    }
    static constexpr bool IsFloatCompatible()
    {
        return false;
    }
    typedef MoleculeGuacamol Type;
};

size_t ML<MoleculeGuacamol>::m_size=1024;

auto GenerateMLValue(MoleculeGuacamol res,at::Tensor tensor)
{
    return ML<MoleculeGuacamol>(tensor);
};

template <typename Accessor>
void GenerateMLValue(MoleculeGuacamol res,Accessor tensor)
{
};

