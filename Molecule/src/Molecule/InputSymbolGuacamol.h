#pragma once
#include "Molecule/MoleculeGuacamol.h"
struct InputSymbolGuacamol
{
    size_t m_id;
    InputSymbolGuacamol(char character)
    {
        std::string str;
        str.push_back(character);
        m_id=MoleculeGuacamol::symbols[str];
    };
    static size_t size()
    {
        return MoleculeGuacamol::symbols.size();
    }
};

template <>
class ML<InputSymbolGuacamol>: public at::Tensor
{
    public:
    static size_t size()
    {
        return MoleculeGuacamol::symbols.size();
    }
    ML()
    {
        
    }
    ML(at::Tensor embedded):at::Tensor(embedded)
    {
        
    }
    static constexpr bool IsFloatCompatible()
    {
	return false;
    }
    typedef InputSymbolGuacamol Type;
};

auto GenerateMLValue(const InputSymbolGuacamol& res,at::Tensor tensor)
{
    tensor[res.m_id]=1;
    return ML<InputSymbolGuacamol>(tensor);
};

template <typename Accessor>
void GenerateMLValue(const InputSymbolGuacamol& res,Accessor tensor)
{
    tensor[res.m_id]=1;
};
