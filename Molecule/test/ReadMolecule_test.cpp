#include <gtest/gtest.h>
#include <Molecule/Molecule.h>

TEST(ReadMolecule,Read)
{
    auto data=CSV2Molecule("/Data/Data/Molecule/QM9/Train.csv");
    std::cout<<Molecule::symbols.size()<<std::endl;
    std::for_each(Molecule::symbols.begin(),Molecule::symbols.end(),[](auto elem)
    {
        std::cout<<elem<<std::endl;
    });
    auto dataTest=CSV2Molecule("/Data/Data/Molecule/QM9/Test.csv");
    std::cout<<Molecule::symbols.size()<<std::endl;
    std::for_each(Molecule::symbols.begin(),Molecule::symbols.end(),[](auto elem)
    {
        std::cout<<elem<<std::endl;
    });
    auto dataVal=CSV2Molecule("/Data/Data/Molecule/QM9/Val.csv");
    std::cout<<Molecule::symbols.size()<<std::endl;
    std::for_each(Molecule::symbols.begin(),Molecule::symbols.end(),[](auto elem)
    {
        std::cout<<elem<<std::endl;
    });
}
