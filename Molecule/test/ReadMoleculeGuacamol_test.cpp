#include <gtest/gtest.h>
#include <Molecule/MoleculeGuacamol.h>

TEST(ReadMoleculeGuacamol,Read)
{
    auto data=CSV2MoleculeGuacamol("/Data/Data/Molecule/Guacamol/guacamol_v1_train.smiles");
    std::cout<<MoleculeGuacamol::symbols.size()<<std::endl;
    std::for_each(MoleculeGuacamol::symbols.begin(),MoleculeGuacamol::symbols.end(),[](auto elem)
    {
        std::cout<<elem<<std::endl;
    });
    auto dataTest=CSV2MoleculeGuacamol("/Data/Data/Molecule/Guacamol/guacamol_v1_test.smiles");
    std::cout<<MoleculeGuacamol::symbols.size()<<std::endl;
    std::for_each(MoleculeGuacamol::symbols.begin(),MoleculeGuacamol::symbols.end(),[](auto elem)
    {
        std::cout<<elem<<std::endl;
    });
    auto dataVal=CSV2MoleculeGuacamol("/Data/Data/Molecule/Guacamol/guacamol_v1_valid.smiles");
    std::cout<<MoleculeGuacamol::symbols.size()<<std::endl;
    std::for_each(MoleculeGuacamol::symbols.begin(),MoleculeGuacamol::symbols.end(),[](auto elem)
    {
        std::cout<<elem<<std::endl;
    });
}

