#include <gtest/gtest.h>
#include <rdkit/GraphMol/SmilesParse/SmilesParse.h>
#include <GraphMol/MolStandardize/MolStandardize.h>
#include <RDGeneral/Invariant.h>
#include <GraphMol/RDKitBase.h>
#include <GraphMol/ROMol.h>
#include <GraphMol/SmilesParse/SmilesParse.h>
#include <GraphMol/SmilesParse/SmilesWrite.h>
#include <GraphMol/FileParsers/FileParsers.h>
#include <RDGeneral/FileParseException.h>
#include <GraphMol/FileParsers/MolSupplier.h>
#include <set>

TEST(rdkit,LoadData)
{
    RDKit::SmilesToMol("C1C2C3C2C2C4OC12C34");
}

TEST(rdkit,RandomMolecule)
{
    auto molecule=RDKit::SmilesToMol("C1C2C3C2C2C4OC12C34");
    std::cout<<"original "<<"C1C2C3C2C2C4OC12C34"<<std::endl;
    std::set<std::string> m_set;
    for(int i=0;i<1000000;++i)
    {
        auto string=RDKit::MolToSmiles(*molecule,true,false,-1,true,false,false,true);
        m_set.insert(string);
    }
    std::cout<<"size "<<m_set.size()<<std::endl;
    
}
