cmake_minimum_required (VERSION 3.8)
project (OOPML C CXX)

add_definitions(-fconcepts -march=core2)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_CURRENT_SOURCE_DIR}/../OOPML)
include(CodeCoverage)

enable_testing()

find_package(RDkit)
find_package (Threads)

set(RDkit_Include_Dirs "${RDKit_INCLUDE_DIRS}")

set(RDkit_Library_Dirs "${RDKit_LIBRARY_DIRS}")
set(RDkit_Libraries "${RDKIT_LIBRARIES}")

add_library(rdkit_libraries INTERFACE)
target_link_libraries(rdkit_libraries INTERFACE ChemReactions FileParsers SmilesParse Depictor RDGeometryLib RDGeneral SubstructMatch Subgraphs MolDraw2D GraphMol DistGeometry DistGeomHelpers MolAlign Optimizer ForceField ForceFieldHelpers Alignment ForceField  MolTransforms EigenSolvers Threads::Threads)

link_directories(${RDkit_Library_Dirs})

add_subdirectory(../OOPML OOPML/build EXCLUDE_FROM_ALL)

add_executable(SmileGRUQM9 ${CMAKE_CURRENT_SOURCE_DIR}/src/Main/main_SmileGRUQM9.cpp)
target_include_directories(SmileGRUQM9 PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/src ${RDkit_Include_Dirs})
target_link_libraries(SmileGRUQM9 rdkit_libraries)
addlibraries(SmileGRUQM9)

add_executable(SmileGRUGuacamol ${CMAKE_CURRENT_SOURCE_DIR}/src/Main/main_SmileGRUGuacamol.cpp)
target_include_directories(SmileGRUGuacamol PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/src ${RDkit_Include_Dirs})
target_link_libraries(SmileGRUGuacamol rdkit_libraries)
addlibraries(SmileGRUGuacamol)
