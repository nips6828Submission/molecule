from registry.gitlab.com/nips6828submission/rdkit as rdkit

from registry.gitlab.com/nips6828submission/base
MAINTAINER nips6828@gmail.com
env NVIDIA_VISIBLE_DEVICES=void
copy --from=rdkit / /
run apt-get update -y
workdir /workspace/Molecule
copy Molecule . 
run mkdir -p build
workdir /workspace/Molecule/build
run cmake -DCMAKE_BUILD_TYPE=Release ..
run make -j $(nproc)
run ldconfig
run mkdir -p /Data/Data
copy MoleculeData /Data/Data/Molecule
env NVIDIA_VISIBLE_DEVICES=all
