To download the docker container do:

```bash
docker pull registry.gitlab.com/nips6828submission/molecule:latest
```

To enter the container:
```bash
docker run --runtime=nvidia -it registry.gitlab.com/nips6828submission/molecule:latest
```

To execute the experiment for QM9:
```bash
./SmileGRUQM9 --task=mu
```
For guacamol:
```bash
./SmileGRUGuacamol --task=logP
```

For help in QM9:
```bash
./SmileGRUQM9 --help
```

For help in guacamol:
```bash
./SmileGRUGuacamol --help
```